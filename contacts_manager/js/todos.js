(function () {
    var TODOS = [
        {
            id: 1,
            name: "Take a break!"
        }, {
            id: 2,
            name: "Eat lunch!"
        }, {
            id: 3,
            name: "Work bitch!"
        }
    ];

    can.fixture("GET /todos", function () {
        return [TODOS]; // these brackets cost me about 45mind-1hour .... They werent added in tutorial...
    });
    can.fixture("GET /todos/{id}", function (request) {
        return TODOS[(+request.data.id) - 1]
    });
    can.fixture("POST /todos", function (request) {
        var id = TODOS.length + 1;
        TODOS.push($.extend({ id: id }, request.data));
        return { id: id };
    });
    can.fixture("PUT /todos/{id}", function (request) {
        $.extend(TODOS[(+request.data.id) - 1], request.data);
        return {};
    });
    can.fixture("DELETE /todos/{id}", function (request) {
        return {};
    });
})();

var Todo = can.Model({
    findAll: 'GET /todos',
    findOne: 'GET /todos/{id}',
    create: 'POST /todos',
    update: 'PUT /todos/{id}',
    destroy: 'DELETE /todos/{id}'
}, {});

varTodos = can.Control({
    init: function () {
        this.element.html(can.view('views/todosView.ejs', todos));
    }
});

var Todos = can.Control({
    init: function (element, options) {
        Todo.findAll({}, function (todos) {
            element.html(can.view('views/todosView.ejs', todos));
        })
    },
    "li click": function(li,event){
        if (!li.hasClass('nav-header')){ // li except nav-header
            var todo = li.data('todo');
            li.trigger('selected', todo);
        }
    },
    "li .destroy {Events.destroy}": function( el, event){
        var todo = el.closest('li').data('todo');
        todo.destroy();
        event.stopPropagation();
    }
});

var Editor = can.Control({
    todo: function(todo) {
        this.options.todo = todo;
        this.on();
        this.setName();
        this.element.show();
    },
    setName: function() {
        this.element.val(this.options.todo.name)
    },
    "change": function(){
        var todo = this.options.todo;
        todo.attr('name', this.element.val())
            .save();
    },
    "{todo} destroyed": function(){
        this.element.hide();
    }
});


Events = {destroy: "click"};

var Routing = can.Control({
    init: function(){
        can.route('', { category: 'all' });
        this.editor = new Editor("#editor");
        new Todos("#todos");
    },
    "route": function(){
        this.editor.element.hide();
    },
    "/:id route": function(data){
        var editor = this.editor;
        Todo.findOne(data, function(todo){
            editor.todo(todo);
        })
    },
    "filter/:category/:id route": function(data){
        var editor = this.editor;
        Todo.findOne(data, function(todo){
            editor.todo(todo);
        })
    },
    "li selected": function( el, ev, todo ) {
        can.route.attr('id', todo.id);
    }
});

new Routing(document.body);
can.route.ready();