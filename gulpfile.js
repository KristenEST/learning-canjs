var gulp = require('gulp');
var server = require('gulp-server-livereload');
 
gulp.task('webserver', function() {
  gulp.src('contacts_manager')
    .pipe(server({
      livereload: true,
      directoryListing: false,
      open: true
    }));
});